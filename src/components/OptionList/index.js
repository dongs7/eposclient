import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import Badge from '@material-ui/core/Badge'
import IconButton from '@material-ui/core/IconButton'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

// Icons 
import CheckboxIcon from '@material-ui/icons/CheckBoxOutlined'
import RadioButtonIcon from '@material-ui/icons/RadioButtonCheckedOutlined'
import TextFieldIcon from '@material-ui/icons/TextFieldsOutlined'
import DropdownIcon from '@material-ui/icons/ListAltOutlined'
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import DeleteChoiceIcon from '@material-ui/icons/DeleteOutlineOutlined';
import DeleteOptionIcon from '@material-ui/icons/Delete';

const listStyle = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
      },
    nested:{
        paddingLeft: theme.spacing(4)   
    }
}))

const getTypeIcon = type => {
    switch(type){
        case 'CHECKBOX':
             return <CheckboxIcon />
        case 'RADIO':
            return <RadioButtonIcon />
        case 'DROPDOWN':
            return <DropdownIcon />
        default:
            return <TextFieldIcon />
    }
}

const OptionList = ({ option, handleOptionRemove, optionIndex, handleChoiceRemove }) => {
    const classes = listStyle()
    const [open, setOpen] = useState(false)

    const handleOpen = e => {
        setOpen(!open)
    }

    console.log(option)
    return(
        <>
        {
            option.choices ?
            <>
                <ListItem button onClick={handleOpen}>
                    <ListItemIcon>
                        <Badge badgeContent={option.choices.length} color="secondary">
                            {getTypeIcon(option.type)}
                        </Badge>
                    </ListItemIcon>
                    <ListItemText primary={`${option.type} - ${option.name}`} secondary={open ? 'Click to collapse' : 'Click to expand'}/>
                    <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="delete" onClick={handleOptionRemove(`${option.type}-${option.name}`)}> 
                            <DeleteOptionIcon />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding dense>
                        {
                            option.choices && 
                            option.choices.map((choice,idx) => (
                                <ListItem key={idx} button className={classes.nested}>
                                    <ListItemIcon>
                                        <StarBorder />
                                    </ListItemIcon>
                                    <ListItemText primary={choice.text} secondary={`C$ ${choice.priceModifier}`}/>
                                    <ListItemSecondaryAction>
                                        <IconButton edge="end" aria-label="delete" onClick={handleChoiceRemove(`${option.type}-${option.name}`, choice)} size="small">
                                            <DeleteChoiceIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))
                        }
                    </List>
                </Collapse>
            </>
            :
            <ListItem button>
                <ListItemIcon>
                    {getTypeIcon(option.type)}
                </ListItemIcon>
                <ListItemText primary={`${option.type} - ${option.name}`} />
                <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="delete" onClick={handleOptionRemove(`${option.type}-${option.name}`)}> 
                            <DeleteOptionIcon />
                        </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        }
            
        </>
    )
}

export default OptionList;