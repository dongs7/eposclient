import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Select from '@material-ui/core/Select'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear'

const searchStyle = makeStyles(theme => ({
    margin:{
        margin: theme.spacing(1),
    },
    divider: {
        height: 28,
        margin: 4,
    },
    form:{
        flex:1
    },
    input:{

    }
}))

const SearchBar = ({ select, filter, onSubmit,  ...rest }) => {

    const classes = searchStyle()
    const [fieldValue, setFieldValue] = useState('')

    const handleFilterType = e => {
        rest.selectProps.onChange(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()
        onSubmit(fieldValue)
    }

    const handleChange = e => {
        setFieldValue(e.target.value)
    }

    const handleClear = () => {
        setFieldValue('')
    }

    return(
        <AppBar position="static" color="transparent">
            <Toolbar>
                {select &&
                    <Select value={rest.selectProps.value} onChange={handleFilterType}>
                        {rest.selectProps.filters.map((filter, idx) => (
                            <MenuItem key={idx} value={filter}>{filter}</MenuItem>
                        ))}
                    </Select>
                }
                <Divider className={classes.divider} orientation="vertical" />
                <form onSubmit={handleSubmit} className={classes.form}>
                    <InputBase
                        autoFocus
                        className={classes.margin}
                        placeholder='Search Here'
                        inputProps={{ 'aria-label': 'naked' }}
                        onChange={handleChange}
                        value={fieldValue}
                        fullWidth
                    />
                </form>
                {
                    fieldValue !== '' &&
                    <IconButton size='small' onClick={handleClear}>
                        <ClearIcon />
                    </IconButton>
                }
                <Divider className={classes.divider} orientation="vertical" />
                <IconButton size='small' onClick={handleSubmit}>
                    <SearchIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
    )
}

export default SearchBar;

// TODO:; Add logic if the search field has the same value as the previous one (ex. dont execute next lines..)