import React from 'react';

import { makeStyles } from '@material-ui/core/styles' 
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Checkbox from '@material-ui/core/Checkbox'

const headerStyle = makeStyles(theme => ({
    header:{
        fontWeight:'bold',
        fontStyle:'italic'
    }
}))

const SortableHeader = ({ checkbox, headers, toggleAll,  onRequestSort, order, orderBy, checkboxProps }) => {
    const classes = headerStyle()
    const createSortHandler = property => event => {
        // console.log(property)
        onRequestSort(property);
      };
    return(
        <TableHead>
            <TableRow>
                {checkbox && 
                    <TableCell padding="checkbox">
                        <Checkbox 
                           checked={checkboxProps.totalItems.length > 0 && (checkboxProps.checkedItems.length === checkboxProps.totalItems.length)}
                           indeterminate={(checkboxProps.checkedItems.length > 0 && checkboxProps.checkedItems.length < checkboxProps.totalItems.length)}
                           onChange={toggleAll} 
                        />
                    </TableCell>
                }
                {headers.map(header => (
                    <TableCell key={header.id} align={header.align} className={classes.header}>
                        <TableSortLabel
                            active={orderBy === header.id}
                            direction={orderBy === header.id ? order : 'asc'}
                            onClick={createSortHandler(header.id)}
                        >
                            {header.name}
                        </TableSortLabel>
                    </TableCell>
                ))
                }
            </TableRow>
        </TableHead>
    )
}

export default SortableHeader;

// TODO:: Finish sorting 