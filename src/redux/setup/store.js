import { createStore, applyMiddleware, compose } from "redux";
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'
import rootReducer from "../modules";
import thunk from "redux-thunk";
// import apiResponseInterceptor from '../middlewares/interceptor'

export const history = createBrowserHistory()

const isDev = process.env.NODE_ENV === "development" ? true : false;

const devTools = isDev && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const composeEnhancers = devTools || compose;

const middlewares = [thunk];
export const store = createStore(
  rootReducer(history),
  composeEnhancers(applyMiddleware(routerMiddleware(history), ...middlewares))
);
