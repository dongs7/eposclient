// Auth Reducer

import api from 'utils/api'
import { handleActions, createActions } from 'redux-actions'
import { push } from 'connected-react-router'
import { produce } from 'immer'

const initState = { 
    isAuthenticated: false,
    isLoading: false,
    isAdmin: false
}

const { 
    authLoading, 
    authSuccess,
    authFailure,
    authReset
} = createActions({
    AUTH_LOADING: isLoading => ({ isLoading }),
    AUTH_SUCCESS: result => ({ result }),
    AUTH_FAILURE: error => ({ error }),
    AUTH_RESET: undefined
})

const auth = handleActions({
    [authLoading]: (state, { payload: { isLoading }}) => {
        return produce(state, draft => {
            draft.isLoading = isLoading
        })
    },
    [authReset]: () => initState

}, initState)

export const req_login = user => async dispatch => {
    try{
        const res = await api.post('/ba/admin/login', user)
        console.log(res)
        dispatch(push('/admin'))
    }catch(err){
        console.log('err while logging you in ', err.response.data)
    }
}

export const req_logout = () => async dispatch => {
    console.log('logout called from reducer')
}

export default auth;