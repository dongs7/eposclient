// Auth Reducer

import api from 'utils/api'
import { handleActions, createActions } from 'redux-actions'
import { produce } from 'immer'
import { get_store_menu } from './menu'

const initState = { 
    isLoading: false
}

const { 
    optionLoading, 
    optionSuccess,
    optionFailure,
    optionReset
} = createActions({
    OPTION_LOADING: isLoading => ({ isLoading }),
    OPTION_SUCCESS: result => ({ result }),
    OPTION_FAILURE: error => ({ error }),
    OPTION_RESET: undefined
})

const option = handleActions({
    [optionLoading]: (state, { payload: { isLoading }}) => {
        return produce(state, draft => {
            draft.isLoading = isLoading
        })
    },
    [optionReset]: () => initState

}, initState)

export const apply_options = (storeId, ids, options) => async (dispatch, getState) => {
    const { menu } = getState()
    console.log(menu)
    dispatch(optionLoading(true))
    const data = { 
        ids: ids,
        options: options
    }
    console.log(data)
    try{    
        const res = await api.post(`/ba/admin/applyOptions/${storeId}`, data)
        // console.log(res)
        dispatch(get_store_menu(storeId)) // update all items of the store

    }catch(err){
        console.log('err while updating options.. ', err.response.data)
    }
    dispatch(optionLoading(false))
    // console.log(options)
}

export const update_product = (storeId, productId, data) => async (dispatch, getState) => {
    console.log('Update product from redux option')
    console.log(storeId, productId)
    console.log(data)
    const { menu } = getState()
    try{
        const res = await api.put(`/ba/admin/${storeId}/update/${productId}`, data)
        if(res.data.updateCount === 1){
            dispatch(get_store_menu(storeId))
        }
    }catch(err){
        console.log('err while updating the product')
    }
}

export default option;