// Code Reducer

import api from 'utils/api'
import { handleActions, createActions } from 'redux-actions'
import { push } from 'connected-react-router'
import { produce } from 'immer'

const initState = { 
    isLoading: false,
    isLoaded: false,
    isVerified: false
}

const { 
    codeLoading, 
    codeLoaded,
    codeVerified,
    codeReset
} = createActions({
    CODE_LOADING: isLoading => ({ isLoading }),
    CODE_LOADED: result => ({ result }),
    CODE_VERIFIED: result => ({ result }),
    CODE_RESET: undefined
})

const code = handleActions({
    [codeLoading]: (state, { payload: { isLoading }}) => {
        return produce(state, draft => {
            draft.isLoading = isLoading
        })
    },
    [codeLoaded]:(state, {payload: { result }}) => {
        return produce(state, draft => {
            draft.isLoaded = result
        })
    },
    [codeVerified]:(state, { payload: { result }}) => {
        return produce(state, draft => {
            draft.isVerified = result
        })
    },
    [codeReset]: () => initState

}, initState)

export const req_code = storeId => async dispatch => {
    dispatch(codeLoading(true))
    try{
        const res = await api.get(`/ba/admin/options/getcode/${storeId}`)
        await dispatch(codeLoaded(true))
    }catch(err){
        console.log('err while getting a code ', err.response.data)
    }
    dispatch(codeLoading(false))
}

export const verify_code = code => async dispatch => {
    try{
        const res = await api.post(`/ba/admin/options/verify`, {code: code})
        dispatch(codeVerified(true))
    }catch(err){
        console.log('err while verifying a code ,', err.response.data)
    }
}

export default code;