// Menu Reducer

import api from 'utils/api'
import { handleActions, createActions } from 'redux-actions'
import { produce } from 'immer'
import partition from 'lodash/partition'
import { normalizeEnabled, normalizeDisabled, normalizeMenus } from 'utils/normalize'

const initState = { 
    isLoading: false,
    byCategories:undefined,
    byMenus:undefined,
}

const {
    menuSetLoading,
    menuSetMenus,
    menuReset
} = createActions({
    MENU_SET_LOADING: isLoading => ({ isLoading }),
    MENU_SET_MENUS: menus => ({ menus }),
    MENU_RESET: undefined
})

const menu = handleActions({
    [menuSetLoading]: (state, {payload: { isLoading }}) => {
        return produce(state, draft => {
            draft.isLoading = isLoading
        })
    },
    [menuSetMenus]: (state, { payload: { menus }}) => {
        return produce(state, draft => {
            console.log(menus[0])
            draft.byCategories = normalizeEnabled(menus[0])
            draft.byMenus = normalizeMenus(menus[0])
            draft.options = normalizeDisabled(menus[1])
        })
    },
    [menuReset]: () => initState
}, initState)

export const get_store_menu = storeId => async dispatch => {
    console.log('GETSTORE MENU CALLEDs')
    dispatch(menuSetLoading(true))
    try{
        const res = await api.get(`/ba/admin/allprodinfo/${storeId}`)
        const groupCategory = await partition(res.data.products, category => category.enabled)
        dispatch(menuSetMenus(groupCategory))
    }catch(err){
        console.log('err while getting store menu ', err.response.data)
    }
    dispatch(menuSetLoading(false))
    // console.log('We are going to get ', storeId, ' menus')
}

export const reset_menu = () => async dispatch => {
    dispatch(menuReset())
}

export default menu;