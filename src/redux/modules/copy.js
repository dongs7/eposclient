// Copy Store Reducer

import api from 'utils/api'
import { handleActions, createActions } from 'redux-actions'
import { produce } from 'immer'

const initState = { 
    isLoading: false,
    selectedStore:undefined,
    storeList:[],
    categoryInfo:{

    }
}

const { 
    copyLoading,
    copyAllStore,
    copySetActive,
    // copyFailure,
    copyReset,
} = createActions({
    COPY_LOADING: isLoading => ({ isLoading }),
    COPY_ALL_STORE: stores => ({ stores }),
    COPY_SET_ACTIVE: store => ({ store }),
    COPY_FAILURE: error => ({ error }),
    COPY_RESET: undefined
})

const copy = handleActions({
    [copyLoading]: (state, { payload: { isLoading }}) => {
        return produce(state, draft => {
            draft.isLoading = isLoading
        })
    },
    [copyAllStore]: (state, { payload: { stores }}) => {
        return produce(state, draft => {
            draft.storeList = stores
        })
    },
    [copySetActive]: (state, { payload: { store }}) => {
        return produce(state, draft => {
            draft.selectedStore = store
        })
    },
    [copyReset]: (state, action) => {
        return {
            ...initState,
            storeList: state.storeList
        }
    }

}, initState)

export const get_stores = () => async dispatch => {
    dispatch(copyLoading(true))
    try{
        const res = await api.get('/ba/admin/stores')
        dispatch(copyAllStore(res.data))
    }catch(err){
        console.log('err while getting all store ', err.response.data)
    }
    dispatch(copyLoading(false))
}

export const set_store = store => async dispatch => {
    dispatch(copySetActive(store))
}

export const reset_store = () => async dispatch => {
    dispatch(copyReset())
}

export default copy;