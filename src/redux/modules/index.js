import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import auth from './auth'
import store from './store'
import menu from './menu'
import code from './code'
// import detail from './detail'
import option from './option'
import copy from './copy'

export default (history) => combineReducers({
    router: connectRouter(history),
    auth,
    store,
    menu,
    code,
    // detail,
    option,
    copy
})