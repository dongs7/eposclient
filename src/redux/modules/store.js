// Store Reducer

import api from 'utils/api'
import { handleActions, createActions } from 'redux-actions'
import { produce } from 'immer'

const initState = { 
    isLoading: false,
    selectedStore:undefined,
    storeList:[]
}

const { 
    storeLoading,
    storeAllStore,
    storeSetActive,
    storeFailure,
    storeReset,
    storeHardReset,
} = createActions({
    STORE_LOADING: isLoading => ({ isLoading }),
    STORE_ALL_STORE: stores => ({ stores }),
    STORE_SET_ACTIVE: store => ({ store }),
    STORE_FAILURE: error => ({ error }),
    STORE_RESET: undefined,
    STORE_HARD_RESET: undefined
})

const store = handleActions({
    [storeLoading]: (state, { payload: { isLoading }}) => {
        return produce(state, draft => {
            draft.isLoading = isLoading
        })
    },
    [storeAllStore]: (state, { payload: { stores }}) => {
        return produce(state, draft => {
            draft.storeList = stores
        })
    },
    [storeSetActive]: (state, { payload: { store }}) => {
        return produce(state, draft => {
            draft.selectedStore = store
        })
    },
    [storeReset]: (state, action) => {
        return {
            ...initState,
            storeList: state.storeList
        }
    },
    [storeHardReset]: (state, action) => {
        return {...initState}
    }

}, initState)

export const get_stores = () => async dispatch => {
    dispatch(storeLoading(true))
    try{
        const res = await api.get('/ba/admin/stores')
        dispatch(storeAllStore(res.data))
    }catch(err){
        console.log('err while getting all store ', err.response.data)
    }
    dispatch(storeLoading(false))
}

export const set_store = store => async dispatch => {
    dispatch(storeSetActive(store))
}

export const reset_store_soft = () => async dispatch => {
    dispatch(storeReset())
}

export const reset_store_hard = () => async dispatch => {
    dispatch(storeHardReset())
}

export default store;