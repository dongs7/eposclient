import { normalize, schema } from 'normalizr'

const enabledCategories = new schema.Entity('categories', {}, {idAttribute: 'id'})
const enabledCategoriesList = [enabledCategories]
export const normalizeEnabled = enabled => {
    let categories = []
    let excludes = ['dinein menu', 'dine-in menu'] // Add exclusions here
    enabled.filter(enabledCategories => excludes.includes(enabledCategories.name.toLowerCase()) ? null : categories.push(enabledCategories))
    return normalize(categories, enabledCategoriesList)
}

const disabledCategories = new schema.Entity('options', {}, {idAttribute: 'id'})
const disabledCategoryList = [disabledCategories]
export const normalizeDisabled = disabled => {
    let options = []
    let categoryToOption = ['others'] // Add Option categories
    disabled.filter(disabledCategory => categoryToOption.includes(disabledCategory.name.toLowerCase()) ? options.push(...disabledCategory.items) : null)
    return normalize(options, disabledCategoryList)
}

const menusFromEnabledCategories = new schema.Entity('menus', {}, {idAttribute:'id'})
const menusFromEnabledCategoriesList = [menusFromEnabledCategories]
export const normalizeMenus = enabled => {
    let merged = enabled.reduce((acc,x) => {
        let temp = x.items ? x.items : []
        // (a[0].items = a[0].items || []).push(...x.items);
        acc.push(...temp)
        return acc;
    }, [])
    return normalize(merged, menusFromEnabledCategoriesList)
}

// Option normalizr
const optionsFromMenu = new schema.Entity('options', {}, {idAttribute: (value) => value ? `${value.type}-${value.name}` : null})
const optionsFromMenuSchema = [optionsFromMenu]
export const normalizeOptions = options => normalize(options, [optionsFromMenuSchema])