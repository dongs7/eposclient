export const defaultEnabledCategoryField = data => {
    return {
        "name": data.name,
        "nameTranslated": {
            "en": data.name
        },
        "description": "",
        "descriptionTranslated": {
            "en": ""
        },
        "enabled": true,
        "orderBy": 10,
    }
}