export const setType = index => {
    switch(index){
        case 0: return 'CHECKBOX'
        case 1: return 'SELECT'
        case 2: return 'RADIO'
        case 3: return 'TEXTFIELD'
        default: return null
    }
}

const optionWrapper = (type, name) => {
    return{
        "name": name,
        "type": type,
        "required": false
    }
}

export const optionForm = data => {
    return {
        "priceModifierType": "ABSOLUTE",
        "text": data.name,
        "priceModifier": data.price
    }
}

export const setOption = (type, name, choices=null) => {
    // console.log(type, name, choices)
    let options = optionWrapper(setType(type), name)
    switch(type){
        case 0:{
            options.choices = [...choices]
            return options
        }
        case 3: return options
        default:{
            options.choices = [...choices]
            options.defaultChoice = 0
            return options
        }
    }
}

export const addToExistingOption = (type, option, choicess=null ) => {
    // console.log(choicess)
    // console.log(type, option, choicess)

    let tempOption = {}
    // console.log(tempOption)
    switch(type){
        case 3: return option
        default:{
            tempOption = {
                ...option,
                choices: [...option.choices, ...choicess]
            }
            console.log(tempOption)
            return tempOption
        }
    }
}

export const mergeCheckedMenus = data => {
    let temp = {
        ids:[],
        options:[]
    }
    for(let [key,value] of Object.entries(data)){
        console.log(value)
        if(value.ids && value.options){
            temp.ids.push(...value.ids)
            temp.options.push(...value.options)
        }
    }
    console.log(temp)

    
    return temp
}

// Finish options