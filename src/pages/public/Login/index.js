import React, { useState } from 'react';

import { useDispatch } from 'react-redux'
import { req_login } from 'redux/modules/auth'


const Login = () => {
    const dispatch = useDispatch()

    const [user, setUser] = useState({
        username:'',
        password:''
    })

    const handleLogin = e => {
        e.preventDefault()
        dispatch(req_login(user))
    }

    const handleChange = e => {
        // console.log(e.target)
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    return(
        <div>
            <form onSubmit={handleLogin}>
                <input value={user.id} name='username' onChange={handleChange}/>
                <input value={user.password} name='password' type="password" onChange={handleChange}/>
                <input type="submit" value="login"/>
            </form>
        </div>
    )
}

export default Login;