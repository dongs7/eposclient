import React, { useState, useEffect } from 'react';

// MUI
import Button from '@material-ui/core/Button'

// Redux
import { get_stores, reset_store_hard, reset_store_soft, set_store } from 'redux/modules/store'
import { get_store_menu, reset_menu } from 'redux/modules/menu'
import { useSelector, useDispatch } from 'react-redux'

// Components
import Select from 'react-select'

const CopyStore = props => {
    const dispatch = useDispatch()
    const menuLoading = useSelector(state => state.menu.isLoading)
    const allStores = useSelector(state => state.store.storeList)
    const selectedStore = useSelector(state => state.store.selectedStore)
    
    const handleStore = async store => {
        dispatch(reset_store_soft())
        if(!store){
            dispatch(reset_menu())
            return;
        }
        await dispatch(set_store(store))
        dispatch(get_store_menu(store.externalStoreId))
    }

    useEffect(() => {
        dispatch(get_stores())
        return () => {
            console.log('reset store')
            dispatch(reset_store_hard())
        }
    },[])
    
    return(
        <div>
            <Select
                options={allStores}
                getOptionValue={option => option['externalStoreId']}
                getOptionLabel={option => option['storeName']}
                onChange={handleStore}
                isClearable
                isLoading={menuLoading}
                isDisabled={menuLoading}
                styles={{
                    // Fixes the overlapping problem of the component
                    menu: provided => ({ ...provided, zIndex: 9999 })
                    }}

            />
            <div>
                {
                    menuLoading ? 
                    "Getting information from the selected store..."
                    :
                    !menuLoading && selectedStore ?  
                        <Button>Extract data from {selectedStore.storeName}</Button>
                        :
                        "Select store to be referenced"
                }
            </div>
        </div>
    )
}

export default CopyStore;