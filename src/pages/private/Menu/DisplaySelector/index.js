import React, { useState, useEffect } from 'react';

// Redux
import { useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/core/styles'
import Fab from '@material-ui/core/Fab';

import ByCategories from '../ByCategories'
import ByMenus from '../ByMenus'
import ByItem from '../ByItem'
import OptionHandler from '../OptionHandler'

import AddIcon from '@material-ui/icons/Add'

const useStyles = makeStyles(theme => ({
    fab:{
        position:'fixed',
        bottom:0,
        right:0,
        margin: '2em'
    }
}))

const DisplaySelector = ({ checked, isUpdatable, selectedStore }) => {
    const classes = useStyles()
    
    const byMenus = useSelector(state => state.menu.byMenus)
    const byCategories = useSelector(state => state.menu.byCategories)

    const [optionOpen, setOptionOpen] = useState(false)
    // const [selectedMenuItem, setSelectedMenuItem] = useState('') 

    const [checkedMenus, setCheckedMenus] = useState({
        byMenus:{
            ids:[],
            options:[]
        },
        byCategories:{}
    })

    const handleToggleAll = data => {
        if(checked){
            // checked from categories
            setCheckedMenus({
                ...checkedMenus,
                byCategories: data
            })
        }else{
            setCheckedMenus({
                ...checkedMenus,
                byMenus:{
                    ids: data.ids,
                    options: data.options
                }
            })
        }
    }

    const handleToggleSingle = data => {
        if(checked){
            // checked from categories
            // console.log(data)
            setCheckedMenus({
                ...checkedMenus,
                byCategories: data
            })
        }else{
            setCheckedMenus({
                ...checkedMenus,
                byMenus:{
                    ids: data.ids,
                    options: data.options
                }
            })
        }
    }

    const openOption = () => {
        if(!optionOpen){
            setOptionOpen(true)
        }
    }

    const closeOption = () => {
        setOptionOpen(false)
    }

    // const handleSelectedMenuItem = menu => e => {
    //     setSelectedMenuItem(menu)
    // }

    return(
        <div>
            {
                checked ? 
                byCategories && 
                <ByCategories 
                    menus={byCategories}
                    checkedMenus={checkedMenus.byCategories}
                    handleToggleAll={handleToggleAll}
                    handleToggleSingle={handleToggleSingle}
                    isUpdatable={isUpdatable}
                    // handleSelectedMenuItem={handleSelectedMenuItem}
                />
                :
                byMenus &&
                <ByMenus
                    menus={byMenus}
                    checkedMenus={checkedMenus.byMenus}
                    handleToggleAll={handleToggleAll}
                    handleToggleSingle={handleToggleSingle}
                    isUpdatable={isUpdatable}
                    // handleSelectedMenuItem={handleSelectedMenuItem}
                />
            }
            {
                // isUpdatable &&
                <React.Fragment>
                    <Fab className={classes.fab} size='small' onClick={openOption}>
                        <AddIcon />
                    </Fab>
                    {/* TODO:: move option handler */}
                    <OptionHandler
                        checked={checked}
                        selectedStore={selectedStore}
                        open={optionOpen}
                        handleClose={closeOption}
                        checkedMenus={checked ? checkedMenus.byCategories : checkedMenus.byMenus}
                    />
                </React.Fragment>
            }
        </div>
    )
}

export default DisplaySelector;