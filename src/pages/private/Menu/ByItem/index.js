import React, { useState, useEffect } from 'react';

//redux
import { useDispatch, useSelector } from 'react-redux'
import { update_product } from 'redux/modules/option'

import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List'
import Tooltip from '@material-ui/core/Tooltip';


// Icons
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import UndoIcon from '@material-ui/icons/UndoOutlined'
import CheckIcon from '@material-ui/icons/CheckCircleOutline'

// Components
import OptionList from 'components/OptionList'

// Image
import NoImage from 'assets/noimage.png'

const detailStyle = makeStyles(theme => ({
    header:{
        fontWeight:'bold'
    },
    card: {
        width: 400
      },
    media:{
        height:240
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
      },
      expandOpen: {
        transform: 'rotate(180deg)',
      },
      innerContent:{
        maxHeight:300,
        overflow:'auto'
      }
}))

const ByItem = ({ selected, open, onClose, isUpdatable}) => {
    const classes = detailStyle()
    const dispatch = useDispatch()
    const optionLoading = useSelector(state => state.option.isLoading)
    const selectedStore = useSelector(state => state.store.selectedStore)
    const [expanded, setExpanded] = useState(false);
    const [tempOptions, setTempOptions] = useState([])
    const [tempDescription, setTempDescription] = useState('')
    const [undo, setUndo] = useState([])

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const handleClose = () => {
        setExpanded(false)
        onClose()
    }

    const handleOptionRemove = typeName => async e => {
        console.log(typeName)
        let temp = tempOptions.filter(option => `${option.type}-${option.name}` !== typeName)
        setUndo(prev => [...prev, tempOptions])
        setTempOptions(temp)
    }

    const handleUndo = () => {
        let temp = [...undo]
        setTempOptions(temp.pop())
        setUndo(temp)
    }

    const handleChoiceRemove = (typeName, selectedChoice) => e => {
        // console.log(typeName , ' and ', choiceIndex)
        let tempOption;
        let targetOption = [...tempOptions]
        let targetOptionIndex = tempOptions.indexOf(tempOptions.filter(option => `${option.type}-${option.name}` === typeName)[0])
        console.log(targetOptionIndex)
        if(targetOptionIndex !== -1){
            const found = targetOption[targetOptionIndex]
            tempOption = { 
                ...found,
                choices: found.choices.filter(choice => choice !== selectedChoice)
            }
            targetOption[targetOptionIndex] = tempOption
        }

        setUndo(prev => [...prev, tempOptions])
        setTempOptions(targetOption)
    }

    const handleApply = () => {
        console.log('apply clicked')
        // console.log(selectedStore.externalStoreId)
        // console.log(selected.id)
        const data = { 
            options: tempOptions,
            description: tempDescription
        }
        dispatch(update_product(selectedStore.externalStoreId, selected.id, data))
    }

    useEffect(() => {
        if(selected !== undefined){
            console.log('this run')
            setTempOptions(selected.options)
            setTempDescription(selected.description)
        }
    }, [selected])

    // console.log(selected)
    if(selected && tempOptions){
        return(
            <Dialog onClose={handleClose} open={selected !== ''}>
                {/* <DialogTitle className={classes.header}>{selected.name}</DialogTitle> */}
                {/* <DialogContent> */}
                    <Card className={classes.card}>
                        <CardMedia
                            className={classes.media}
                            image={selected.thumbnailUrl || NoImage}
                            title={selected.name}
                        />
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {selected.name}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {selected.description || 'No product description'}
                        </Typography>
                        </CardContent>
                    
                    {
                        tempOptions.length > 0 &&
                        <>
                            <CardActions disableSpacing>
                                <IconButton onClick={handleApply} disabled={undo.length === 0}>
                                    <CheckIcon />
                                </IconButton>
                                <IconButton onClick={handleUndo} disabled={undo.length === 0}>
                                    <UndoIcon />
                                </IconButton>
                                <IconButton
                                    className={clsx(classes.expand, {
                                        [classes.expandOpen]: expanded,
                                    })}
                                    onClick={handleExpandClick}
                                    aria-expanded={expanded}
                                    aria-label="show more"
                                    >
                                    <ExpandMoreIcon />
                                </IconButton>
                            </CardActions>
                            <Collapse in={expanded} timeout="auto" unmountOnExit>
                                <CardContent className={classes.innerContent}>
                                <List dense>
                                    {tempOptions.map((option,idx) => (
                                        <OptionList key={idx} option={option} optionIndex={idx} handleOptionRemove={handleOptionRemove} handleChoiceRemove={handleChoiceRemove}/>
                                    ))}
                                </List>
                                </CardContent>
                            </Collapse>
                        </>
                    }
                    
                    </Card>
                {/* </DialogContent> */}
            </Dialog>
        )
    }
    return null;
}

export default ByItem;