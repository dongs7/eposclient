import React, { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux'
import { get_stores, reset_store_soft, reset_store_hard, set_store } from 'redux/modules/store'
import { get_store_menu, reset_menu } from 'redux/modules/menu'

import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import LinearProgress from '@material-ui/core/LinearProgress'


import Select from 'react-select'
import IOSSwitch from 'components/IOSSwitch';
import DisplaySelector from './DisplaySelector'


const Switcher = props => { 
    const { checked, handleChange, leftLabel, rightLabel } = props
    return(
        <Typography component="div">
        <Grid component="label" container alignItems="center" spacing={1}>
            <Grid item>{leftLabel}</Grid>
          <Grid item>
            <IOSSwitch
              checked={checked}
              onChange={handleChange}
            //   value="checkedC"
            />
          </Grid>
          <Grid item>{rightLabel}</Grid>
        </Grid>
      </Typography>
    )
}

const Menu = props => {
    const dispatch = useDispatch()
    const menuLoading = useSelector(state => state.menu.isLoading)
    const allStores = useSelector(state => state.store.storeList)
    const selectedStore = useSelector(state => state.store.selectedStore)

    const [checked, setChecked] = useState(false)
    useEffect(() => {
        dispatch(get_stores())
        return() => {
            console.log('reset store')
            dispatch(reset_store_hard())
        }
    }, [])

    const handleStore = async store => {
        dispatch(reset_store_soft())
        if(!store){
            dispatch(reset_menu())
            return;
        }
        await dispatch(set_store(store))
        dispatch(get_store_menu(store.externalStoreId))
    }

    const handleChange = () => {
        setChecked(!checked)
    }

    return(
        <div>
            <Select
                options={allStores}
                getOptionValue={option => option['externalStoreId']}
                getOptionLabel={option => option['storeName']}
                onChange={handleStore}
                isClearable
                isLoading={menuLoading}
                isDisabled={menuLoading}
                styles={{
                    // Fixes the overlapping problem of the component
                    menu: provided => ({ ...provided, zIndex: 9999 })
                    }}

            />
            { !menuLoading ?
                !selectedStore ? 
                    <Typography>No Store Selected</Typography> 
                    :
                    <React.Fragment>
                        <div>
                            <Switcher
                                checked={checked}
                                handleChange={handleChange}
                                leftLabel='Uncate'
                                rightLabel='Cate'
                            />
                        </div>
                        <DisplaySelector 
                            checked={checked}
                            isUpdatable={selectedStore.updateOption}
                            selectedStore={selectedStore}
                        />
                    </React.Fragment>
               :
               <LinearProgress color="primary" />
            }
        </div>
    )
}

export default Menu;