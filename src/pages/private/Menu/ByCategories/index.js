import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Checkbox from '@material-ui/core/Checkbox'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination'

// Component
import PaginationAction from 'components/PaginationAction'

// icons
import CheckIcon from '@material-ui/icons/CheckCircleOutline'
import ClearIcon from '@material-ui/icons/ClearOutlined'

const expansionStyles = makeStyles(theme => ({
    root: {
      width: '100%',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    summaryWrapper:{
        display:'flex',
        alignItems:'center',
        flex:1
    }
  }));

  const tableHeader = () => [
    {name:'Name', align:"left"},
    {name:'Price', align:"right"},
    {name:'Options', align:"right"},
    {name:'SKU', align:"right"},
]
// const initCategories = data => data.reduce((a,b) => (a[b]={ids:[], options:[]},a),{})

const ByCategories = React.memo(({ menus, checkedMenus, handleToggleAll, handleToggleSingle }) => {
// const ByCategories = ({ menus, checkedMenus, handleToggleAll, handleToggleSingle, handleSelectedMenuItem }) => {
    // console.log(checkedMenus)
    const classes = expansionStyles()
    const headers = tableHeader()
    const [expanded, setExpanded] = useState(false)
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(5)
    const [selectedMenu, setSelectedMenu] = useState('')

    const handleExpand = selectedCategoryId => (e, isExpanded) =>  {
        if(e.target.type !== 'checkbox'){
            setExpanded(isExpanded ? selectedCategoryId : false)
        }
    }

    const toggleAllInCategory = (categoryId) => e =>{
         let combined = {...checkedMenus}
         console.log(e.target.checked)
         if(e.target.checked){
            combined = { 
                ...combined,
                [categoryId]:{
                    ids: menus.entities.categories[categoryId].items.map(item => item.id) || [],
                    options: menus.entities.categories[categoryId].items.map(item => item.options.map(option => option)) || []
                }
            }
         }else{
            combined = { 
                ...combined,
                [categoryId]:{
                    ids: [],
                    options: []
                }
            }
         }
         handleToggleAll(combined)
    }

    const toggleSingleInCategory = (categoryId, menuId, menuOptions) => e => {
        let combined = {...checkedMenus}
        const selectedIndex = checkedMenus[categoryId] ? checkedMenus[categoryId].ids.indexOf(menuId) : -10
        // console.log(menuOptions)
        if(selectedIndex === -10){ // no category found
            combined = {
                ...combined, 
                [categoryId]:{
                    ids: [menuId],
                    options:[menuOptions]
                }
            }
        }else if(selectedIndex === -1){ // Found category, but item not found
            combined = {
                ...combined, 
                [categoryId]:{
                    ids: [...combined[categoryId].ids,menuId],
                    options:[...combined[categoryId].options, menuOptions]
                }
            }
        }else{
            combined[categoryId].ids.splice(selectedIndex, 1)
            combined[categoryId].options.splice(selectedIndex, 1)
        }

        handleToggleSingle(combined)
    }

    const isSelected = (categoryId, menuId) => checkedMenus[categoryId] ? checkedMenus[categoryId].ids.indexOf(menuId) !== -1 : false
    // const isSelected = (categoryId, menuId) => checkedMenusByCategory[categoryId].ids.indexOf(menuId) !== -1

    const handleSelectedMenu = menu => e =>{
        console.log(menu)
        setSelectedMenu(menu)
    }

    // TODO:: Fix this
    const handleChangePage = (e,newPage)  =>{
        console.log(e) 
        setPage(newPage)
    }
    
    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        // setPage(0);
    };

    useEffect(() => {
        return () => {
            console.log('unmount category')
        }
    },[])

    return(
        <div className={classes.root}>
            {menus.result.map(categoryId => (
                <ExpansionPanel key={categoryId} expanded={categoryId === expanded} onChange={handleExpand(categoryId)}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1bh-content"
                        id="panel1bh-header"
                    >
                        <Checkbox
                            // onClick={handlePanelCheckbox}
                            // checked={menus.entities.categories[categoryId].items.length > 0 && (checkedMenus[categoryId] ? checkedMenus[categoryId].ids.length === menus.entities.categories[categoryId].items : false)}
                            checked={checkedMenus[categoryId] ? (menus.entities.categories[categoryId].items.length > 0 && checkedMenus[categoryId].ids.length === menus.entities.categories[categoryId].items.length): false}
                            indeterminate={checkedMenus[categoryId] && (checkedMenus[categoryId].ids.length > 0 && checkedMenus[categoryId].ids.length < menus.entities.categories[categoryId].items.length)}
                            onChange={toggleAllInCategory(categoryId)}
                        />
                        <div className={classes.summaryWrapper}>
                            <Typography className={classes.heading}>{menus.entities.categories[categoryId].name}</Typography>
                            {/* <Typography className={classes.secondaryHeading}>{menus.entities.categories[categoryId].items.length || 0}</Typography> */}
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <TableContainer component={Paper}>
                            <Table size="small" >
                                <TableHead>
                                    <TableRow>
                                        <TableCell padding="checkbox">
                                            {/* <Checkbox 
                                                // checked={menus.entities.categories[categoryId].items.length > 0 && (checkedMenusByCategory[categoryId].length === menus.entities.categories[categoryId].items)}
                                                checked={true}
                                                onChange={toggleAllInCategory}
                                            /> */}
                                        </TableCell>
                                        {headers.map(header => (
                                            <TableCell key={header.name} align={header.align} className={classes.header}> 
                                                {header.name}
                                            </TableCell>
                                            ))}
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {menus.entities.categories[categoryId].items && menus.entities.categories[categoryId].items
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map(menu => {
                                        const selectedIndex = isSelected(categoryId, menu.id)
                                        // console.log(selectedIndex)
                                        return(
                                            <TableRow key={menu.id} onClick={handleSelectedMenu(menu)}>
                                                <TableCell padding="checkbox">
                                                    <Checkbox 
                                                        checked={selectedIndex}
                                                        onChange={toggleSingleInCategory(categoryId, menu.id, menu.options)}
                                                        />
                                                </TableCell>
                                                <TableCell align="left">{menu.name}</TableCell>
                                                <TableCell align="right">{menu.price}</TableCell>
                                                <TableCell align="right">{menu.options.length ? <CheckIcon /> : <ClearIcon />}</TableCell>
                                                <TableCell align="right">{menu.sku}</TableCell>
                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TablePagination 
                                            rowsPerPageOptions={[5, { value: -1, label: 'All' }]}
                                            ActionsComponent={PaginationAction}
                                            colSpan={3}
                                            count={menus.entities.categories[categoryId].items && menus.entities.categories[categoryId].items.length || 0}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            onChangePage={handleChangePage}
                                            onChangeRowsPerPage={handleChangeRowsPerPage}
                                        />
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </TableContainer>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            ))}
        </div>
   )
})

export default ByCategories;

// TODO:: Create keys for categories