import React from 'react';

import { makeStyles } from '@material-ui/core/styles'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const expStyle = makeStyles(theme => ({
    root:{ 
        width:'100%'
    },
    column:{
        flexBasis:'33.3%'
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
      },
      secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
      },
}))

const OptionExpansion = ({ isOptionSelected, type, name, checkedOptionIds, options }) => {
    const classes = expStyle()
    console.log(options)
    console.log(checkedOptionIds)
     console.log(type, name)
    if(isOptionSelected){
        return(
            <div className={classes.root}>
                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1c-content"
                        id="panel1c-header"
                    >
                        <div className={classes.column}>
                            <Typography className={classes.heading}>{type}</Typography>
                        </div>
                        <div className={classes.column}>
                            <Typography className={classes.secondaryHeading}>{name}</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        {checkedOptionIds.map(id => (
                            <p key={id}>{options.entities.options[id].name}</p>
                        ))}
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        )
    }
    else{
        return "Options are not selected."
    }
}

export default OptionExpansion;