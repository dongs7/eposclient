import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import TextField from '@material-ui/core/TextField'
import CircularProgress from '@material-ui/core/CircularProgress'
import Backdrop from '@material-ui/core/Backdrop'
// Components
import OptionTypeSelector from './OptionTypeSelector'
import OptionTable from './OptionTable'
import OptionReview from './OptionReview'

// utils
import { setType, optionForm, setOption, addToExistingOption, mergeCheckedMenus } from 'utils/option'
import { normalizeOptions } from 'utils/normalize'

// Redux
import { useSelector, useDispatch } from 'react-redux'
import { req_code, verify_code } from 'redux/modules/code'
import { apply_options } from 'redux/modules/option'

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  loading:{
    position:'fixed',
    width:'100%',
    height:'100%',
    zIndex:2000,
    background: 'rgba(0,0,0,0.5)',
    left:0,
    top:0
  },
  progressWrapper:{
    position:'absolute',
    top:'50%',
    left:'50%'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1000,
    color: '#000',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const selections = [
  {name: 'byId'}
]

const optionSteps = () => [
    'Set Option Type and Name',
    'Choose Options to be applied',
    'Review Option and Verify'
]

const optionContentIndex = {
  0:'OptionType',
  1:'Options',
  2:'OptionName',
  3:'Review'
}

const OptionHandler = ({ checked, open, handleClose, selectedStore, checkedMenus }) => {
  const mergedChecked = checked ? mergeCheckedMenus(checkedMenus) : checkedMenus
  const classes = useStyles();
  const dispatch = useDispatch();
  const steps = optionSteps()

  // verification code
  const codeLoading = useSelector(state => state.code.isLoading)
  const codeLoaded = useSelector(state => state.code.isLoaded)
  const codeVerified = useSelector(state => state.code.isVerified)
  
  // options
  const options = useSelector(state => state.menu.options)
  const optionLoading = useSelector(state => state.option.isLoading)

  const [activeStep, setActiveStep] = useState(0)
  const [optionType, setOptionType] = useState(0)
  const [optionName, setOptionName] = useState('')
  const [code, setCode] = useState('')
  const [checkedOptions, setCheckedOptions] = useState({
    ids:[],
    options:[] // TODO:: Change thos to key-value
  })

  // const [checkedMenuModified, setCheckMenuModified] = useState({
  //   ids: checked ? 
  // })

  const handleActiveStep = index => e => setActiveStep(index)
  const handleOptionType = e => setOptionType(e.target.value)

  const handleOptionName = e => setOptionName(e.target.value)

  const handleOptionCheck = optionId => {
    console.log('option checked and create Form ', optionId)
    const optionIndex = checkedOptions.ids.indexOf(optionId)
    if(optionIndex === -1){
      const converted = optionForm(options.entities.options[optionId])
      setCheckedOptions({
        ids: [...checkedOptions.ids, optionId],
        options:[...checkedOptions.options, converted] 
      })
    }else{
      const tempIds = [...checkedOptions.ids]
      const tempOptions = [...checkedOptions.options]
      tempIds.splice(optionIndex,1)
      tempOptions.splice(optionIndex, 1)
      setCheckedOptions({
        ids: tempIds,
        options: tempOptions
      })
    }
  }

  const handleAllOptionCheck = data => {
    let converted = data.ids.map(optionId => optionForm(options.entities.options[optionId]))
    console.log(converted)
    setCheckedOptions({
      ids: data.ids,
      options: converted
    })
  }

  const getCode = () => {
    dispatch(req_code(selectedStore.externalStoreId))
  }

  const handleCode = e => {
    e.preventDefault()
    dispatch(verify_code(code))
  }

  const handleChangeCode = e => {
    setCode(e.target.value)
  }

  // Need to fix this:
  // if type is not found loop will be done need to fix rghis
  const applyOption = async () => {
    console.log('start applying option')
    let optionToBeAdded = setOption(optionType, optionName, checkedOptions.options)
    let temp = []

      for(let i in mergedChecked.ids){
        console.log('I value ', i)
        let new_ItemOption = []
        if(mergedChecked.options[i].length){
          for(let j in mergedChecked.options[i]){
            console.log('start inner, ', j)
            let added = false;
            let current_ItemOption = mergedChecked.options[i][j]
            
            let typeToBeInsert = setType(optionType)
            let typeCurrent = current_ItemOption["type"]
            
            let nameToBeInsert = optionName
            let nameCurrent = current_ItemOption["name"]
            
            // check type
            if(typeCurrent === typeToBeInsert){
              if(nameCurrent === nameToBeInsert){ // same name
                console.log('Same Name, Add this to the new option')
                new_ItemOption.push(addToExistingOption(optionType, current_ItemOption, checkedOptions.options))
                added = true;
              }else{ // diff name
                console.log('Diff Name')
                console.log(current_ItemOption)
                new_ItemOption.push(current_ItemOption)
              } 
            }else{ // diff type
              console.log('Different Type')
              if(nameCurrent !== nameToBeInsert){
                console.log('Different Name')
                new_ItemOption.push(current_ItemOption)
              }
            }
            console.log(parseInt(j), ' and ', mergedChecked.options[i].length-1, ' and ', added)
            if(added === false && (parseInt(j) === mergedChecked.options[i].length - 1)){
              console.log('Done New Option Add ', optionToBeAdded)
              new_ItemOption.push(optionToBeAdded)
              temp.push(new_ItemOption)
            }

            if(added){
              temp.push(new_ItemOption)
            }
            // console.log(new_ItemOption)
            
          }
        }else{
          temp.push([optionToBeAdded])
        }
      }
      console.log(temp)
      
    dispatch(apply_options(selectedStore.externalStoreId, mergedChecked.ids, temp))
    // let temp = []
    // for (let i in mergedChecked.ids){
    //   let innerTemp = []
    //   console.log('outer')
    //   let added = false
    //   for( let j in mergedChecked.options[i] ){
    //     console.log(`Inner temp after ${j} loop : `, innerTemp)
    //     console.log('inner ', i , j,mergedChecked.options[i],mergedChecked.options[i][j] )
    //     let option = mergedChecked.options[i][j]
    //     if((option["type"] === setType(optionType)) && (setType(optionType) !== "TEXTFIELD")){
    //       if(option["name"] === optionName){
    //         console.log('same type same name, append choies ' , setType(optionType), optionName, option["name"])
    //         innerTemp.push(addToExistingOption(optionType, option, checkedOptions.options))
    //       }else{
    //         console.log('same type diff name, append choies ' , setType(optionType), optionName, option["name"])
    //         innerTemp.push(option)
    //         // await innerTemp.push(setOption(optionType, optionName, checkedOptions.options))
    //       }
    //       console.log('added?')
    //       added = true
    //       // added = true
    //     }else{
    //       console.log('this running too ?')
    //       innerTemp.push(option)
    //     }
    //     console.log(innerTemp)
    //   } // inner forkoop
    //   console.log('runrun..')
    //   // innerTemp.push(setOption(optionType, optionName, checkedOptions.options))

    //   if(!added){
    //     innerTemp.push(setOption(optionType, optionName, checkedOptions.options))
    //   }
    //   console.log('right above temp ', innerTemp)
    //   temp.push(innerTemp)
    // }
    // dispatch(apply_options(selectedStore.externalStoreId, mergedChecked.ids, temp))
  }

  // debug reset options
  const applyReset = () => {
    let temp = []
    for(let i in mergedChecked.ids){
      temp.push([])
    }
    dispatch(apply_options(selectedStore.externalStoreId, mergedChecked.ids,temp))
  }

  // console.log(optionLoading)
  return (
    <div>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
                {selectedStore.storeName.toUpperCase()} Options
            </Typography>
            {
              codeLoading ? 
                <CircularProgress />
                :
                codeLoaded ?
                  codeVerified ?
                    <>
                      <Button autoFocus color="inherit" onClick={applyOption}>
                        apply
                      </Button> 
                      <Button autoFocus color="inherit" onClick={applyReset}>
                        reset
                      </Button> 
                    </>
                    :
                    <form onSubmit={handleCode}>
                      <TextField
                        value={code} 
                        onChange={handleChangeCode}
                      />
                    </form>
                :
                <Button autoFocus color="inherit" onClick={getCode}>
                  get Code
                </Button>
            }
          </Toolbar>
        </AppBar>
        <DialogContent>
            <Stepper activeStep={activeStep} orientation='vertical'> 
                {steps.map((label, index) => {
                    return(
                        <Step key={index}>
                            <StepLabel onClick={handleActiveStep(index)}>{label}</StepLabel>
                            <StepContent>
                              {index === 0 && <OptionTypeSelector type={optionType} onChangeType={handleOptionType} onChangeName={handleOptionName} optionName={optionName}/>}
                              {index === 1 && <OptionTable options={options} toggleAllOptionCheck={handleAllOptionCheck} toggleOptionCheck={handleOptionCheck} checkedOptions={checkedOptions}/>}
                              {index === 2 && <OptionReview type={setType(optionType)} name={optionName} options={options} checkedOptionIds={checkedOptions.ids}/>}
                            </StepContent>
                        </Step>
                    )
                })}
            </Stepper>
            
        </DialogContent>
      </Dialog>
      <Backdrop className={classes.backdrop} open={optionLoading}>
          <CircularProgress color="secondary" />
      </Backdrop>
    </div>
  );
}

export default OptionHandler;

// TODO:: Finish this part!! show code field after loading
// If option type is textfield, disable second stepper