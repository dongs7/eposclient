import React, { useState } from 'react';

import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

// Components
import OptionExpansion from '../OptionExpansion'

const OptionReview = ({ type, name, checkedOptionIds, options }) => {

    const [open, setOpen] = useState(false)
    
    const handleOpen = () => {
        setOpen(true)
    }

    const isOptionSelected = type && name && checkedOptionIds.length

    return(
        <div>
            <OptionExpansion isOptionSelected={isOptionSelected} type={type} name={name} checkedOptionIds={checkedOptionIds} options={options}/>
            {
                !open ? 
                    <Button onClick={handleOpen} disabled={name === ''}>
                        Get Verification Code
                    </Button>
                    :
                    <TextField 
                    />
            }
        </div>
    )
}

export default OptionReview;