import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox'
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination'

// Component
import SearchBar from 'components/SearchBar'
import PaginationAction from 'components/PaginationAction'
import SortableHeader from 'components/SortableHeader'

// utils
import { splitIntoArray } from 'utils/arrays'


const tableStyle = makeStyles(theme => ({
    header:{
        fontWeight:'bold',
        fontStyle:'italic'
    }
}))

const tableHeader = () => [
    {id:'name', name:'Name', align:"left"},
    {id:'price', name:'Price', align:"right"},
    {id:'sku', name:'SKU', align:"right"},
    {id:'id', name:'IDS(test)', align:"right"},
]

const searchFilters = [
    'SKU', 'NAME'
]

function descendingComparator(a, b, orderBy, entities) {
    if (entities[b][orderBy] < entities[a][orderBy]) {
      return -1;
    }
    if (entities[b][orderBy] > entities[a][orderBy]) {
      return 1;
    }
    return 0;
  }
  
  function getComparator(order, orderBy, entities) {
    //   console.log(entities)
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy, entities)
      : (a, b) => -descendingComparator(a, b, orderBy, entities);
  }
  
  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    // console.log(stabilizedThis)
    stabilizedThis.sort((a, b) => {
        // console.log(a, b)
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
  }

const OptionTable = ({ options, toggleOptionCheck, checkedOptions, toggleAllOptionCheck }) => {
    const classes = tableStyle()
    const headers = tableHeader()
    
    // test sorting
    const [order, setOrder] = useState("asc")
    const [orderBy, setOrderBy] = useState("Name")


    const [filteredOptions, setFilteredOptions] = useState(options.result)
    const [filter, setFilter] = useState('SKU')
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(10)

    const isSelected = id => checkedOptions.ids.indexOf(id) !== -1

    const handleChangePage = (e, newPage) => {
        setPage(newPage)
    }
    
    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleFilter = filterType => {
        setFilter(filterType)
    }

    const handleSearch = value => {
        console.log(value)
        let tempFiltered = [...options.result]
        if(value !== ''){
            if(filter === 'SKU'){
                const skus = splitIntoArray(value)
                tempFiltered =  tempFiltered.filter(optionId => skus.indexOf(options.entities.options[optionId].sku) !== -1)    
            }else{
                console.log('name ', value)
                tempFiltered =  tempFiltered.filter(optionId => options.entities.options[optionId].name.toLowerCase().includes(value.toLowerCase()))    
            }
        }
        setFilteredOptions(tempFiltered)
    }

    const toggleSingle = optionId => e => {
        toggleOptionCheck(optionId)
    }

    // TODO:: NEed to fix here!
    const toggleAll = e => {
        let data = { 
            ids : [],
            options : []
        }
        
        if(e.target.checked){
            console.log('checked')
            data.ids = filteredOptions.map(optionId => {
                data.options.push(options.entities.options[optionId].options)
                return optionId
            })
        }
        console.log(options)
        console.log(data)
        toggleAllOptionCheck(data)
    }

    const handleRequestSort = property => {
        console.log(property)
        const isAsc = (orderBy === property) && (order === 'asc')
        setOrder(isAsc ? 'desc' : 'asc')
        setOrderBy(property)
    }

    // console.log(filteredOptions)
    return(
        <TableContainer component={Paper}>
            <SearchBar
                select
                selectProps={{
                    value: filter,
                    filters: searchFilters,
                    onChange: handleFilter
                }}
                // onChange={handleSearch}
                onSubmit={handleSearch}
            />
            <Table size="small">
                {/* <TableHead>
                    <TableRow>
                        <TableCell padding="checkbox">
                            <Checkbox 
                                checked={filteredOptions.length > 0 && (checkedOptions.ids.length === filteredOptions.length)}
                                indeterminate={(checkedOptions.ids.length > 0 && checkedOptions.ids.length < filteredOptions.length)}
                                // onChange={toggleAll}
                                />
                        </TableCell>
                        {headers.map(header => (
                            <TableCell key={header.name} align={header.align} className={classes.header}> 
                                {header.name}
                            </TableCell>
                            ))}
                    </TableRow>
                </TableHead> */}
                <SortableHeader 
                    checkbox
                    checkboxProps={{
                        totalItems: filteredOptions,
                        checkedItems: checkedOptions.ids
                    }}
                    headers={headers}
                    order={order}
                    orderBy={orderBy}
                    onRequestSort={handleRequestSort}
                    toggleAll={toggleAll}
                />
                <TableBody>
                    {
                    stableSort(filteredOptions, getComparator(order, orderBy, options.entities.options))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(optionId => {
                        const seletedIndex = isSelected(optionId)
                        return(
                            <TableRow key={optionId}>
                                <TableCell padding="checkbox">
                                    <Checkbox 
                                        checked={seletedIndex}
                                        onChange={toggleSingle(optionId)}
                                        />
                                </TableCell>
                                <TableCell align="left">{options.entities.options[optionId].name}</TableCell>
                                <TableCell align="right">{options.entities.options[optionId].price}</TableCell>
                                <TableCell align="right">{options.entities.options[optionId].sku}</TableCell>
                                <TableCell align="right">{options.entities.options[optionId].id}</TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination 
                            rowsPerPageOptions={[15, { value: -1, label: 'All' }]}
                            ActionsComponent={PaginationAction}
                            colSpan={3}
                            count={filteredOptions.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    )
}

export default OptionTable;