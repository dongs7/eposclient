import React from 'react';

// MUI
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem';
import InputBase from '@material-ui/core/InputBase'

// Icons
import CheckboxIcon from '@material-ui/icons/CheckBoxOutlined'
import RadioButtonIcon from '@material-ui/icons/RadioButtonCheckedOutlined'
import TextFieldIcon from '@material-ui/icons/TextFieldsOutlined'
import DropdownIcon from '@material-ui/icons/ListAltOutlined'

const typeTabStyle = makeStyles(theme => ({
    root:{
        maxWidth:350
    },
    margin:{
        margin: theme.spacing(1)
    },
    root:{
        display:'flex'
    }
}))


const OptionTypeSelector = ({ onChangeType, type, onChangeName, optionName }) => {
    const classes = typeTabStyle()
    
    return(
        <div className={classes.root}>
            <Select
                onChange={onChangeType}
                value={type}
            >
                <MenuItem value={0}><CheckboxIcon /> Check Box</MenuItem>
                <MenuItem value={1}><DropdownIcon /> Drop Down</MenuItem>
                <MenuItem value={2}><RadioButtonIcon /> Radio Button</MenuItem>
                <MenuItem value={3}><TextFieldIcon /> Text Field</MenuItem>
            </Select>
            <form>
                <InputBase
                    autoFocus
                    className={classes.margin}
                    placeholder='Enter option name'
                    inputProps={{ 'aria-label': 'naked' }}
                    onChange={onChangeName}
                    value={optionName}
                    fullWidth
                />
            </form>
        </div>
    )
}

export default OptionTypeSelector;