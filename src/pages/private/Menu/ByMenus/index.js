import React, { useState, useEffect } from 'react';
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox'
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination'

// Component
import SearchBar from 'components/SearchBar'
import PaginationAction from 'components/PaginationAction'
import ByItem from '../ByItem'

// utils
import { splitIntoArray } from 'utils/arrays'

// icons
import CheckIcon from '@material-ui/icons/CheckCircleOutline'
import ClearIcon from '@material-ui/icons/ClearOutlined'

const tableStyle = makeStyles(theme => ({
    header:{
        fontWeight:'bold',
        fontStyle:'italic'
    },
    menuRow:{
        cursor:'pointer',
        "&:hover": {
            backgroundColor:'rgba(220,220,220,0.3)'
        }
    },
    disabledMenu:{
        backgroundColor:'#F6BABA'
    }
}))

const tableHeader = () => [
    {name:'Name', align:"left"},
    {name:'Price', align:"right"},
    {name:'Options', align:"right"},
    {name:'SKU', align:"right"},
]

const searchFilters = [
    'SKU', 'NAME'
]

const ByMenus = React.memo(({ menus, checkedMenus, handleToggleAll, handleToggleSingle, isUpdatable }) => {
    const classes = tableStyle()
    // const menus = useSelector(state => state.menu.byMenus)
    const headers = tableHeader()
    const [filteredMenus, setFilteredMenus] = useState(menus.result)
    const [filter, setFilter] = useState('SKU')
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(15)
    const [selectedMenuId, setSelectedMenuId] = useState('')
    const [open, setOpen] = useState(false)
    
    const toggleAll = e => {
        let data = { 
            ids : [],
            options : []
        }
        
        if(e.target.checked){
            console.log('checked')
            data.ids = filteredMenus.map(menuId => {
                data.options.push(menus.entities.menus[menuId].options)
                return menuId
            })
        }
        handleToggleAll(data)
    }

    const toggleSingle = menuId => e => {
        console.log(e.target.type)
        console.log('menu Id ', menuId)
        let data = { 
            ids: [...checkedMenus.ids],
            options: [...checkedMenus.options]
        }
        const selectedIndex = data.ids.indexOf(menuId)
        if(selectedIndex === -1){ // not selected
            data.ids.push(menuId)
            data.options.push(menus.entities.menus[menuId].options)
        }else{ // selected
            data.ids.splice(selectedIndex, 1)
            data.options.splice(selectedIndex, 1)
        }
        handleToggleSingle(data)
    }

    const handleSearch = value => {
        let tempFiltered = [...menus.result]
        // console.log(tempFiltered)
        if(value !== ''){
            if(filter === 'SKU'){
                const skus = splitIntoArray(value)
                tempFiltered =  tempFiltered.filter(menuId => skus.indexOf(menus.entities.menus[menuId].sku) !== -1)    
            }else{
                console.log('name ', value)
                tempFiltered =  tempFiltered.filter(menuId => menus.entities.menus[menuId].name.toLowerCase().includes(value.toLowerCase()))    
            }
        }
        setFilteredMenus(tempFiltered)
    }

    const handleFilter = filterType => {
        setFilter(filterType)
    }

    const handleChangePage = (e, newPage) => {
        setPage(newPage)
    }
    
    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const isSelected = id => checkedMenus.ids.indexOf(id) !== -1
    // console.log(checkedMenus.ids.length, ' items selected')

    const handleSelected = menuId => e => {
        if(e.target.type !== 'checkbox'){
            setSelectedMenuId(menuId)
        }
    }

    const handleClose = () => {
        console.log('closed')
        setSelectedMenuId('')
    }

    // useEffect(() => {
    //     return () => {
    //         console.log('unmount')
    //     }
    // })
    return(
        <>
        <TableContainer component={Paper}>
            <SearchBar
                select
                selectProps={{
                    value: filter,
                    filters: searchFilters,
                    onChange: handleFilter
                }}
                // onChange={handleSearch}
                onSubmit={handleSearch}
            />
            <Table size="small" >
                <TableHead>
                    <TableRow>
                        <TableCell padding="checkbox">
                            <Checkbox 
                                checked={filteredMenus.length > 0 && (checkedMenus.ids.length === filteredMenus.length)}
                                indeterminate={(checkedMenus.ids.length > 0 && checkedMenus.ids.length < filteredMenus.length)}
                                onChange={toggleAll}
                                />
                        </TableCell>
                        {headers.map(header => (
                            <TableCell key={header.name} align={header.align} className={classes.header}> 
                                {header.name}
                            </TableCell>
                            ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {filteredMenus
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(menuId => {
                        const seletedIndex = isSelected(menuId)
                        return(
                            <TableRow key={menuId} onClick={handleSelected(menuId)} className={clsx({[classes.menuRow]: menus.entities.menus[menuId].enabled, [classes.disabledMenu]: !menus.entities.menus[menuId].enabled} )}> 
                                <TableCell padding="checkbox">
                                    <Checkbox 
                                        checked={seletedIndex}
                                        onChange={toggleSingle(menuId)}
                                        />
                                </TableCell>
                                <TableCell align="left">{menus.entities.menus[menuId].name}</TableCell>
                                <TableCell align="right">{menus.entities.menus[menuId].price}</TableCell>
                                <TableCell align="right">{menus.entities.menus[menuId].options.length ? <CheckIcon /> : <ClearIcon />}</TableCell>
                                <TableCell align="right">{menus.entities.menus[menuId].sku}</TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination 
                            rowsPerPageOptions={[15, { value: -1, label: 'All' }]}
                            ActionsComponent={PaginationAction}
                            colSpan={3}
                            count={filteredMenus.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
            <ByItem
                isUpdatable={isUpdatable}
                selected={menus.entities.menus[selectedMenuId]}
                onClose={handleClose}
            />
        </TableContainer>
        </>
    )
})

export default ByMenus;
