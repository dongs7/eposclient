import React from 'react';
import { Route, Switch } from 'react-router-dom'

// Route Pages
import Layout from 'components/Layout'
import Menu from '../Menu'
import Store from '../Store'
import Home from '../Home'
import CopyStore from '../CopyStore'

// Icons
import HomeIcon from '@material-ui/icons/AssessmentOutlined'
import StoreIcon from '@material-ui/icons/StoreOutlined'
import MenuIcon from '@material-ui/icons/ChromeReaderModeOutlined'
import CopyIcon from '@material-ui/icons/FileCopyOutlined'

// Redux
import { useDispatch } from 'react-redux'
import { req_logout } from 'redux/modules/auth'

const menus = [
    {id:'menu_over', name:'Overview', link:'', icon: <HomeIcon />},
    {id:'menu_stores', name:'Stores', link:'store', icon: <StoreIcon />},
    {id:'menu_menus', name:'Menus', link:'menu', icon: <MenuIcon />},
    {id:'menu_copy', name:'Copy Store', link:'copystore', icon: <CopyIcon />}
]

const PrivateRouteHandler = ({ match }) => {
    const dispatch = useDispatch()

    const handleLogout = () => {
        dispatch(req_logout())
        // console.log('Logout clicked')
    }

    return(
        <Layout menus={menus} baseUrl={match.path} handleLogout={handleLogout}>
            <Switch>
                <Route exact path={`${match.path}/`} component={Home} />
                <Route exact path={`${match.path}/menu`} component={Menu} />
                <Route exact path={`${match.path}/store`} component={Store} />
                <Route exact path={`${match.path}/copystore`} component={CopyStore} />
            </Switch>
        </Layout>
    )
}

export default PrivateRouteHandler;