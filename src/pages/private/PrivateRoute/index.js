import React, { Component } from 'react';

import { Redirect, Route } from 'react-router-dom'


const PrivateRoute = ({ component: Component, ...rest }) => {
    const auth = true
    return(
        <Route 
            {...rest}
            render={({location, ...others}) => 
            auth ? <Component {...others}/>
            :
            <Redirect 
                to={{
                    pathname: '/login',
                    state:{ from: location }
                }}
            />
            }
        />
    )
}

export default PrivateRoute;