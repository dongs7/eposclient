import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom'

import Login from 'pages/public/Login'
import PrivateRoute from 'pages/private/PrivateRoute'
import PrivateRouteHandler from 'pages/private/PrivateRouteHandler'

const App = () => {
  return(
    <Switch>
      <Redirect exact from='/' to='/login' />
      <Route exact path='/login' component={Login} />
      <PrivateRoute path='/admin' component={PrivateRouteHandler} />
    </Switch>
  )
}

export default App;